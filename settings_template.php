<?php
//Debug
ini_set('error_reporting', E_ALL);  // REMOVE TO TURN DEBUG OFF
ini_set('display_errors', 1);       // REMOVE TO TURN DEBUG OFF

//MySQL settings
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'USERNAME');
define('DB_PASSWORD', 'PASSWORD');
define('DB_DATABASE', 'appdater');
define('DB_PORT', '3307');

//GLOBAL settings
define('XOR_KEY', '<KEY TO ENCODE FILES>');
define('FILES_URL', 'http://<SERVER ADDRESS>/files');
define('APPDATER_PATH', '<YOUR PROJECT PATH>/');
define('SALT', '<PASSWORD SALT>');
?>
